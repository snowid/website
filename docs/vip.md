> 你好欢迎使用PigX,以下内容请仔细阅读       

🌹 pigx 使用说明：    
-  源码地址：[https://gitee.wang/pig](https://gitee.wang/pig) 

🚫 私服使用说明：️
-  账号禁止分享，多终端异常登录会被封号处理
-  账号用户名请保持和QQ一致，不定期清理

🔖 文档、视频
-  [pigx部署快速部署](https://gitee.wang/pig/pigx/src/master/doc/md/deploy.md)
-  [pigx 调用流程原理](参考售后群文档，视频的OAuth原理解析必须看)
-  [生产部署等请参考视频](docs/pigx/vedio.md)  

♻ 关于提问       
- 80%的问题都可以通过文档、或者视频解决。
- 20%的问题请提供异常的解决、日志。  
- 不阅读文档、视频的提问大多数会被忽略，谢谢理解

!> ps： 我发现很多朋友都是不看群聊天。别人提问的问题，或许是你知识盲点。


📡 知识星球，最新的微服务学习  
<img src="_media/qy.png" width="30%" height="30%">   
