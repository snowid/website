?> 基本的RBAC 、数据权限这些通用的功能都有就不比较了   
pigx 通过定制spring security oauth ,对token 的权限控制更加细化    
网关采用最新的webflux 背压 spring cloud gateway 性能更加好  
深度封装LCN，只需一个注解即可解决跨服务的事务问题     

|              | Pig  | Pigx |
| ------------ | :--: | :--: |
| Spring Boot  |   1.5.15.RELEASE   |   2.0.6.RELEASE   |
| Spring Cloud |   Edgware.SR4   |  Finchley.SR2    |
| 代码组件stater |   ❌   |  ✅    |
| 分布式事务   |   ❌   |  ✅    |
| 图形化代码生成  |   ❌   |  ✅    |
| OAuth2上下文传递  |   ❌   |  ✅    |
| Token细化服务  |   ❌   |  ✅    |
| Token强制下线  |   ❌   |  ✅    |
| OAuth2社交登录   |   ❌   |  ✅    |
| webflux   |   ❌   |  ✅    |
| UI无限制路由   |   ❌   |  ✅    |
| UI监控   |   ❌   |  ✅    |
